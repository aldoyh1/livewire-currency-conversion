<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Redirect;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;
use App\Models\User;

class CustomCheckLoggedInState
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next, string ...$guards)
    {
        $Functions = new Functions();

        $AdminUser = User::where('id', Auth::id())->first();

        // echo "<pre>";
        // print_r($AdminUser->name);
        // print_r($AdminUser->surname);
        // exit;

        $guards = empty($guards) ? [null] : $guards;
        foreach ($guards as $guard) {
            if (!Auth::guard($guard)->check()) {
                $Functions->set_login_redirect_url($request->fullUrl());
                return redirect()->route('login');
            }
        }
        return $next($request);
    }
}
