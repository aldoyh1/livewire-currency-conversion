<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Redirect;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;
use App\Models\User;
use App\Models\Currencies as CurrenciesModel;

class CustomCheckMyCurrenciesRealtionships
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next, string ...$guards)
    {
        $currencies = CurrenciesModel::limit(10)->get();
        if(count($currencies) == 0){
            session()->flash('danger','Please create (currencies) first.');
            return redirect()->route('home');
        }

        return $next($request);
    }
}
