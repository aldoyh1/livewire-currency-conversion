<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currencies extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'description',
    ];

    protected $table = 'currencies';

    public function scopeSearch($query, $filters)
    {
        if(isset($filters['search']) && $filters['search'] != ''){
            $query->where('code', 'LIKE', '%' . $filters['search'] . '%');
            $query->orWhere('description', 'LIKE', '%' . $filters['search'] . '%');
        }
    }

}
