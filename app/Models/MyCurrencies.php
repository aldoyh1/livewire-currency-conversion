<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Currencies;


class MyCurrencies extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'currency_id',
        'description',
    ];

    protected $table = 'my_currencies';

    public function currency()
    {
        return $this->belongsTo(Currencies::class, 'currency_id');
    }
}
