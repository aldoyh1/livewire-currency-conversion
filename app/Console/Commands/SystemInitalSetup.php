<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserCreatedMail;
use App\Mail\ManagePeopleMail;
use App\Models\People as PeopleModel;
use App\Models\User as UserModel;
use App\Helpers\CurrencyApiConsumer;
use App\Helpers\Functions;


class SystemInitalSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:initial-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        // =========== testing my emails =========== 
        // $id = 1;
        // // $this->test_people_email($id, "Contact Details Updated", "Your Contact details has been created. See Login below");
        // $this->test_send_new_user_email($id);
        // echo "Done";exit;

        // $base_amount = 100;
        // $from_currency = 'zar';
        // $to_currency = 'gbp';
        // $this->convert_currency($base_amount, $from_currency, $to_currency);
        // exit; 

        //
        $Functions = new Functions();
        
        $this->newLine();
 
        //check if tables exist 
        if(Schema::hasTable('users')){
            $this->newLine();
            $this->newLine();
            $this->info("Please use (yes/no) (Case Sensitive) to answer this question");
            $answer = $this->choice(
                'This will clear project current data, do you wish to continue?',
                ['no' => 'Eh... No. I will keep my current project data thank you.', 'yes' => 'Yes, I want to clear project data'],
            );
            switch($answer){
                case 'no':
                    $this->info("You decided to terminate the command");
                    break;
                case 'yes':
                    $this->do_initial_setup();
                    break;
                default:
                    $this->error("Invalid choice : $answer");
            }
        }else{
            $this->do_initial_setup();
        }

    }

    function do_initial_setup(){

        //get user email address: do_initial_setup() 

        Artisan::call('migrate:refresh');
        
        $user_id = $this->add_initial_user();
        $this->import_currencies();
        $this->add_default_settings($user_id);

        $this->info("System Initial Setup completed successfully");
        $this->newLine();
        

    }

    function convert_currency($base_amount, $from_currency, $to_currency){
        $CurrencyApiConsumer = new CurrencyApiConsumer();
        
        $response = $CurrencyApiConsumer->convert_currency($base_amount, $from_currency, $to_currency);
        

        if($response['status']){
            
        }else{
            $this->info("Currency Import failed");
        }
        $this->info("Currency Import completed successfully");
        $this->newLine();
    }

    function import_currencies(){
        $CurrencyApiConsumer = new CurrencyApiConsumer();
        $response = $CurrencyApiConsumer->get_currency_list();
        if($response['status']){
            foreach($response['data'] as $code => $description){
                DB::table('currencies')->insert([
                    'code' => strtoupper($code),
                    'description' => $description,
                    'created_at' => Carbon::now(),
                ]);
            }
        }else{
            $this->info("Currency Import failed");
        }
        $this->info("Currency Import completed successfully");
        $this->newLine();
    }
    
    function add_default_settings($user_id){
        DB::table('settings')->insert([
            'user_id' => $user_id,
            'system_name' => 'base_currency',
            'description' => 'Base Currnecy',
            'value' => 'ZAR',
            'created_at' => Carbon::now(),
        ]);
        $this->info("Default Settings added successfully");
        $this->newLine();
    }

    function add_initial_user(){

        //$surname = $this->ask('What is your Surname?');
        //$email = $this->ask('What is your email address?');
        //$password = $this->secret('What is your Password?');
        //$confirm_password = $this->secret('Confirm your Password?');

        //========= get name ================
        $input_correct = false;
        $db_name = 'name';
        $input = "";
        $input_label = "Name";
        $rules = "required|255";
        do{
            $input = $this->ask("What is your $input_label?");
            if($input){
                $rules_arr = ["$db_name" => 'required|max:255'];
                $rules_msg_arr = ["$db_name.required" => "Please provide $input_label"];
                $input_correct = $this->input_correct($db_name, $rules_arr, $rules_msg_arr,$input);
            }
        }while(!$input_correct);
        $name = $input;

        //========= get surname ================
        $input_correct = false;
        $db_name = 'surname';
        $input = "";
        $input_label = "Surname";
        $rules = "required|255";
        do{
            $input = $this->ask("What is your $input_label?");
            if($input){
                $rules_arr = ["$db_name" => 'required|max:255'];
                $rules_msg_arr = ["$db_name.required" => "Please provide $input_label"];
                $input_correct = $this->input_correct($db_name, $rules_arr, $rules_msg_arr,$input);
            }
        }while(!$input_correct);
        $surname = $input;

        //========= get email ================
        $input_correct = false;
        $db_name = 'email';
        $input = "";
        $input_label = "Email";
        $rules = "required|255";
        do{
            $input = $this->ask("What is your $input_label?");
            if($input){
                $rules_arr = ["$db_name" => 'required|max:255|email'];
                $rules_msg_arr = ["$db_name.required" => "Please provide $input_label"];
                $input_correct = $this->input_correct($db_name, $rules_arr, $rules_msg_arr,$input);
            }
        }while(!$input_correct);
        $email = $input;

        //========= get password ================
        $input_correct = false;
        $db_name = 'password';
        $input = "";
        $input_label = "Password";
        $rules = "required|255";
        do{
            $input = $this->ask("What is your $input_label?");
            if($input){
                $rules_arr = ["$db_name" => 'required|max:255'];
                $rules_msg_arr = ["$db_name.required" => "Please provide $input_label"];
                $input_correct = $this->input_correct($db_name, $rules_arr, $rules_msg_arr,$input);
            }
        }while(!$input_correct);
        $password = $input;

        //========= get confirm password ================
        $input_correct = false;
        $db_name = 'confirm_password';
        $input = "";
        $input_label = "Password";
        $rules = "required|255";
        do{
            $input = $this->ask("What is your $input_label?");
            if($input){
                $rules_arr = ["$db_name" => 'required|max:255'];
                $rules_msg_arr = ["$db_name.required" => "Please provide $input_label"];
                $input_correct = $this->input_correct($db_name, $rules_arr, $rules_msg_arr,$input);
            }

            //====== extra validation 
            if($password != $input){
                $input_correct = false;
                $this->error("Error:  . Your confirm passwords is inccorect.");
            }
        }while(!$input_correct);
        $confirm_password = $input;

        $this->info('These details were sent to your email:');
        $this->line('Your name is: ' . $name);
        // $this->newLine();
        $this->line('Your surname is: ' . $surname);
        // $this->newLine();
        $this->line('Your email is: ' . $email);
        // $this->newLine();
        $this->line('Your password is: ' . $password);
        $this->newLine();

        //Do insert 
        $user_id = DB::table('users')->insertGetId([
            'name' => $name,
            'surname' => $surname,
            'email' => $email,
            'email_verified_at' => Carbon::now(),
            'created_at' => Carbon::now(),
            'password' => bcrypt($password),
        ]);

        //Send email to this guy with his logins: 
        $recepients_arr[] = ['email' => $email, 'name' => $name." ".$surname];
        $data = [
            'name' => $name,
            'surname' => $surname,
            'email' => $email,
            'password' => $password,
            'login_link' => url('/')."/login",
        ];
        Mail::to($recepients_arr)->send(new NewUserCreatedMail($data));

        return $user_id;

    }

    function input_correct(string $db_name, array $rules_arr = [], array $rules_msg_arr = [], string $input){
        $input_arr[$db_name] = $input;
        $validator = Validator::make($input_arr, $rules_arr, $rules_msg_arr);
        $success = true;
        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            foreach($errors as $error){
                $this->error("Error:  . " . $error[0]);
                $success = false;
            }
        }
        return $success;
    }

    function test_people_email($id, $subject, $message){
        $this_guy = PeopleModel::where('id', $id)->first();
        if($this_guy){
            $recepients_arr[] = ['email' => $this_guy->email, 'name' => $this_guy->name." ".$this_guy->surname];
            $data = [
                'subject' => $subject,
                'name' => $this_guy->name,
                'surname' => $this_guy->surname,
                'message' => $message,
                'details' => [
                    'Name' => $this_guy->name,
                    'Surname' => $this_guy->surname,
                    'Email' => $this_guy->id_number,
                    'password' => $this_guy->mobile_number,
                    'Email' => $this_guy->email,
                    'Date of birth' => $this_guy->date_of_birth,
                ]
            ];
            Mail::to($recepients_arr)->send(new ManagePeopleMail($data));
        }
        
    }

    function test_send_new_user_email($id){
        $this_guy = UserModel::where('id', $id)->first();
        if($this_guy){
            $recepients_arr[] = ['email' => $this_guy->email, 'name' => $this_guy->name." ".$this_guy->surname];
            $data = [
                'name' => $this_guy->name,
                'surname' => $this_guy->surname,
                'email' => $this_guy->email,
                'password' => $this_guy->password,
                'login_link' => url('/')."/login",
            ];
            Mail::to($recepients_arr)->send(new NewUserCreatedMail($data));
        }
        
    }
}
