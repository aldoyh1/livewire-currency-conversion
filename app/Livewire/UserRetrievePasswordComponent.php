<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\User as UserModel;
use Livewire\Attributes\Rule; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserCreatedMail;

class UserRetrievePasswordComponent extends Component
{

    #[Rule('required|max:255')] 
    public $name = '';

    public function render()
    {
        return view('livewire.user-retrieve-password-component')->layout('livewire.layouts.niceadmin-logged-out');
    }

    public function retreive(Request $request)
    {

        $this->validate(); 
        
    }
    
}
