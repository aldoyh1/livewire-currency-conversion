<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Currencies as CurrenciesModel;
use Livewire\Attributes\Rule; 
use Livewire\WithPagination; 
use Livewire\Attributes\Computed;

class CurrenciesComponent extends Component
{

    use WithPagination;
    
    #[Rule('required')]
    public $code = "";
    public $description = "";
    public $search = "";

    public $edit_id = "";
    public $modal_title = "";
    public $btn_text = "";
    public $form_action = "";
    public $records_per_page = 10;
    public $page = 1;

    public function mount(){
        $this->get_presets();
    }

    public function render()
    {
        return view('livewire.currencies-component')->layout('livewire.layouts.niceadmin-logged-in');
    }

    public function create()
    {
        $this->validate();
        $insert = ['code' => $this->code, 'description' => $this->description];
        $record = CurrenciesModel::create($insert);
        session()->flash('success-modal','You have added new record successfully.');
        $this->get_presets();
        $this->reset(['code', 'description']);
    }

    public function update()
    {

        $this->validate();
        $update = ['code' => $this->code, 'description' => $this->description];
        CurrenciesModel::where('id',$this->edit_id)->update($update);
        session()->flash('success-modal','You have updated details successfully.');

    }
    
    public function delete($id)
    {
        $record = CurrenciesModel::where('id',$id)->delete();
        $this->get_presets();
        $this->dispatch('close-modal');
        session()->flash('success','You have deleted the record successfully.');
    }

    public function showModal($action, $id = ""){

        $this->get_presets();

        switch($action){
            case 'add': 
                $this->modal_title = "Add New";
                $this->form_action = "create";
                $this->btn_text = "Submit";
                $this->dispatch('show-modal');
                break;
            case 'view':
                $this->form_action = "view";
                $this->modal_title = "View Details";
                $this->set_edit_record($id);
                $this->dispatch('show-modal');
                break;
            case 'edit':
                $this->dispatch('close-modal');
                $this->form_action = "update";
                $this->modal_title = "Update Details";
                $this->btn_text = "Save";
                $this->set_edit_record($id);
                $this->dispatch('show-modal');
                break;
            case 'del':
                $record = CurrenciesModel::where('id', $id)->first();
                $this->edit_id = $record->id;
                $this->dispatch('show-del-modal');
                break;
        }

    }

    function set_edit_record($id){
        $record = CurrenciesModel::where('id', $id)->first();
        $this->edit_id = $record->id;
        $this->code = $record->code;
        $this->description = $record->description;
    }

    #[Computed]
    function records(){
        $filters = [];
        if($this->search != ''){
            $this->resetPage();
            $filters['search'] = $this->search;
        }
        return CurrenciesModel::search($filters)->orderBy('code', 'asc')->orderBy('description', 'asc')->paginate($this->records_per_page);
        // return CurrenciesModel::where('code', 'LIKE', '%' . $filters['search'] . '%')->orderBy('code', 'asc')->orderBy('description', 'asc')->paginate($this->records_per_page);
    }

    function get_presets(){
        $this->records();
    }
}
