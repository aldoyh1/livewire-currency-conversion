<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Currencies as CurrenciesModel;
use App\Models\MyCurrencies as MyCurrenciesModel;
use App\Models\Settings as SettingsModel;


class HomeComponent extends Component
{

    public $user = [];
    public $name = "";
    public $currencies = [];
    public $my_currencies = [];

    function mount(){
        $user = Auth::user();
        $this->user = $user;
        $this->name = $user->name;
        $this->currencies = CurrenciesModel::count();
        $this->my_currencies = MyCurrenciesModel::where('user_id', $this->get_user_id())->count();
    }

    public function render()
    {
        return view('livewire.home-component',[
            'user' => $this->user,
        ])->layout('livewire.layouts.niceadmin-logged-in');
    }

    protected $listeners = ['reRenderParent'];
    public function reRenderParent()
    {
        session()->flash('success-modal','You have updated your details successfully.');
        $this->mount();
        $this->render();
    }

    function get_user_id(){
        $user = Auth::user();
        return $user->id;
    }
}
