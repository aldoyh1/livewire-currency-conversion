<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\User as UserModel;
use Livewire\Attributes\Rule; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserCreatedMail;
use App\Models\Settings as SettingsModel;

class UserRegisterComponent extends Component
{

    #[Rule('required|max:255')] 
    public $name = '';

    #[Rule('required|max:255')] 
    public $surname = '';
    
    #[Rule('required|email|unique:users')]
    public $email = '';

    #[Rule('required|max:255')] 
    public $password = '';
    #[Rule('required|required_with:password|same:password|max:255')] 
    public $confirm_password = '';
    
    #[Rule('required')] 
    public $terms = '';

    public $auto_login = '';

    public function render()
    {
        return view('livewire.user-register-component')->layout('livewire.layouts.niceadmin-logged-out');
    }

    public function register(Request $request)
    {

        $this->validate(); 
        
        $insert = [
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'email_verified_at' => Carbon::now(),
            'last_login_at' => Carbon::now(),
            'password' => bcrypt($this->password),
        ];

        $user = UserModel::create($insert);
        $this->add_default_settings($user->id);
        $this->send_new_user_email($user->id);

        if ($this->auto_login) {
            Auth::attempt(['email' => $this->email, 'password' => $this->password]);
            $request->session()->regenerate();
            return redirect()->route('home');
        }else{
            session()->flash('success','You have registered successfully.');
            return redirect()->route('login');
        }

    }

    function add_default_settings($user_id){
        $insert = [
            'user_id' => $user_id,
            'system_name' => 'base_currency',
            'description' => 'Base Currnecy',
            'value' => 'ZAR',
            'created_at' => Carbon::now()];
        $user = SettingsModel::create($insert);
    }

    function send_new_user_email($id){
        $this_guy = UserModel::where('id', $id)->first();
        if($this_guy){
            $recepients_arr[] = ['email' => $this_guy->email, 'name' => $this_guy->name." ".$this_guy->surname];
            $data = [
                'name' => $this_guy->name,
                'surname' => $this_guy->surname,
                'email' => $this_guy->email,
                'password' => $this->password,
                'login_link' => url('/')."/login",
            ];
            Mail::to($recepients_arr)->send(new NewUserCreatedMail($data));
        }
        
    }
}
