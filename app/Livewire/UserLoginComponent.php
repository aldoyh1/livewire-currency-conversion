<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Attributes\Rule; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;
use Carbon\Carbon;
use App\Models\User as UserModel;

class UserLoginComponent extends Component
{

    #[Rule('required|email')] 
    public $email = '';

    #[Rule('required')] 
    public $password = '';
    public $remember_me = '';

    public function render()
    {
        return view('livewire.user-login-component')->layout('livewire.layouts.niceadmin-logged-out');
    }

    public function login(Request $request)
    {

        $Functions = new Functions();

        $this->validate(); 
        
        $credentials = [
            'email' => $this->email,
            'password' => $this->password,
        ];

        if (Auth::attempt($credentials)) {

            $user = Auth::user();
            $update = ['last_login_at' => Carbon::now()];
            UserModel::where('id',$user->id)->update($update);
            $request->session()->regenerate();
            session()->flash('success','Welcome back ' . $user->name ." " . $user->surname);
            if($Functions->get_login_redirect_url()){
                return redirect($Functions->get_login_redirect_url());
                $Functions->clear_login_redirect_url();
            }else{
                return redirect()->route('home');
            }
        }else{
            session()->flash('danger','Login failed, invalid email and password');
        }

    }
}
