<?php

namespace App\Livewire\HomeTabs;

use Livewire\Component;
use Livewire\Attributes\Rule; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;
use Carbon\Carbon;
use App\Models\User as UserModel;
use App\Models\MyCurrencies as MyCurrenciesModel;
use App\Models\Currencies as CurrenciesModel;
use App\Models\Settings as SettingsModel;
use App\Helpers\CurrencyApiConsumer;

class HomeTabExchangeCalcComponent extends Component
{

    #[Rule('required|numeric')] 
    public $base_amount = "";
    
    #[Reactive] 
    public $my_conversions = [];

    public $base_currency_desc = "";
    public $setting_base_found = true;
    public $base_currency_found = true;
    public $prerequisite_error_msg = "";
    
    public function render()
    {
        return view('livewire.home-tabs.exchange-calc-component', [
            'my_conversions' => $this->my_conversions,
        ]);
    }

    public function boot(){
        // echo "boot";exit;
    }

    function mount(){
        $base_currency = SettingsModel::where('user_id', $this->get_user_id())->where('system_name','base_currency')->first();
        $my_currencies = MyCurrenciesModel::where('user_id', $this->get_user_id())->count();
        if(!$base_currency){
            $this->prerequisite_error_msg = "Please create base currency under <strong>Settings</strong> with: <code>['system_name' => 'base_currency', 'value' => 'ZAR']</code>";
        }elseif($my_currencies == 0){
            $this->prerequisite_error_msg = 'Please load <a href="'.route('my-currencies').'"><strong>My Currencies</strong></a> first.';
        }else{
            $currency = CurrenciesModel::whereRaw('UPPER(code) = ?', strtoupper($base_currency->value))->first();
            if(!$currency){
                $this->base_currency_found = false;
                $this->prerequisite_error_msg = 'Base currency ('.$base_currency->value.') in Settings was not found in at list of <a href="'.route('manage-currencies').'">Manage Currencies</a>.';
            }else{
                $this->base_currency_desc = $currency->code ." (".$currency->description.")";
            }
        }
    }
    
    public function calc_exchange(Request $request)
    {

        $this->validate(); 
        $CurrencyApiConsumer = new CurrencyApiConsumer();
        $base_currency = SettingsModel::where('user_id', $this->get_user_id())->where('system_name','base_currency')->first();
        $my_currencies = MyCurrenciesModel::where('user_id', $this->get_user_id())
        ->orderBy('description', 'asc')
        ->get();
        $my_conversions = [];
        foreach($my_currencies as $line){
            $my_conversions[$line->currency->code] = [
                'amount' => 0,
                'description' => $line->currency->code . ' ('.$line->description.')',
            ];

            $amount = $this->base_amount;
            $from_currency = $line->currency->code;
            $to_currency = $base_currency->value;
            $response = $CurrencyApiConsumer->convert_currency($amount, $from_currency, $to_currency);
            $my_conversions[$line->currency->code] = [
                'amount' => ($response['data']->converted_amount ?? 0),
                'description' => $line->description,
            ];
        }
        ksort($my_conversions);
        $this->my_conversions = $my_conversions;

        //convert_currency($amount, $from_currency, $to_currency)
        /*
        Array
        (
            [status] => 1
            [message] => It worked
            [data] => stdClass Object
                (
                    [date] => 2023-10-25
                    [zar] => 23.13706438
                    [base_amount] => 100
                    [converted_amount] => 2313.71
                    [rate] => 100
                )

        )
        */

    }

    function get_user_id(){
        $user = Auth::user();
        return $user->id;
    }


}
