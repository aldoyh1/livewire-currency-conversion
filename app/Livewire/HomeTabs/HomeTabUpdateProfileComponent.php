<?php

namespace App\Livewire\HomeTabs;

use Livewire\Component;
use Livewire\Attributes\Rule; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;
use Carbon\Carbon;
use App\Models\User as UserModel;
use App\Livewire\HomeComponent;

class HomeTabUpdateProfileComponent extends Component
{

    #[Rule('required')] 
    public $name = "";

    #[Rule('required')] 
    public $surname = "";

    #[Rule('required')]
    public $email = "";

    public function render()
    {
        return view('livewire.home-tabs.update-profile-component');
    }

    function mount(){
        $user = Auth::user();
        $this->name = $user->name;
        $this->surname = $user->surname;
        $this->email = $user->email;
    }
    
    public function update(Request $request)
    {
        $user = Auth::user();
        $this->validate(); 

        //extra validation on email 
        $this->validate([
            'email' => 'unique:users,email,' . $user->id,
        ]);

        UserModel::where('id', $user->id)
        ->update([
           'surname' => $this->surname,
           'name' => $this->name,
           'email' => $this->email,
        ]);
        
        $this->dispatch('reRenderParent')->to(HomeComponent::class);

    }


}
