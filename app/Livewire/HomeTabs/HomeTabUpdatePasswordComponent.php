<?php

namespace App\Livewire\HomeTabs;

use Livewire\Component;
use Livewire\Attributes\Rule; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;
use Carbon\Carbon;
use App\Models\User as UserModel;
use Illuminate\Support\Facades\Hash;
use App\Mail\UserUpdatePasswordMail;
use Illuminate\Support\Facades\Mail;


class HomeTabUpdatePasswordComponent extends Component
{

    #[Rule('required')] 
    public $current_password = "";

    #[Rule('required|max:255')] 
    public $new_password = '';

    #[Rule('required|required_with:new_password|same:new_password|max:255')] 
    public $confirm_password = '';
    
    public function render()
    {
        return view('livewire.home-tabs.update-password-component');
    }
    
    public function update(Request $request)
    {
        $user = Auth::user();
        $this->validate(); 

        $this->validate([
            'current_password' => [
                'required',
                
                function ($attribute, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->password)) {
                        $fail('Invalid current password');
                    }
                }
            ],
            'new_password' => [
                'required', 'different:current_password'
            ]
        ]);

        UserModel::where('id', $user->id)
        ->update([
           'password' => bcrypt($this->new_password)
        ]);

        $this->send_password_update_email();


        $this->current_password = "";
        $this->new_password = "";
        $this->confirm_password = "";
        session()->flash('success-modal','You have updated your details successfully.');

    }

    function send_password_update_email(){
        //UserUpdatePasswordMail
        $user = Auth::user();
        $name = $user->name;
        $surname = $user->surname;
        $email = $user->email;
        $password = $this->new_password;

        //Send email to this guy with his logins: 
        $recepients_arr[] = ['email' => $email, 'name' => $name." ".$surname];
        $data = [
            'name' => $name,
            'surname' => $surname,
            'email' => $email,
            'password' => $password,
            'login_link' => url('/')."/login",
        ];
        Mail::to($recepients_arr)->send(new UserUpdatePasswordMail($data));

    }


}
