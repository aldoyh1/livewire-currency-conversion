<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Currencies as CurrenciesModel;
use App\Models\MyCurrencies as MyCurrenciesModel;
use Livewire\Attributes\Rule; 
use Livewire\WithPagination; 
use Livewire\Attributes\Computed;
use App\Mail\ManagePeopleMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class MyCurrenciesComponent extends Component
{


    use WithPagination;

    #[Rule('required')]
    public $currency = "";

    #[Rule('required')]
    public $description = "";

    public $edit_id = "";
    public $modal_title = "";
    public $btn_text = "";
    public $form_action = "";
    public $currencies = [];
    public $show_form = false;
    public $records_per_page = 10;

    public function mount(){
        $this->get_presets();
    }

    public function render()
    {
        return view('livewire.my-currencies-component')->layout('livewire.layouts.niceadmin-logged-in');
    }

    public function create()
    {

        $this->validate();
        
        $insert = [
            'user_id' => $this->get_user_id(),
            'currency_id' => $this->currency,
            'description' => $this->description,
        ];

        $record = MyCurrenciesModel::create($insert);
        session()->flash('success-modal','You have added new record successfully.');
        $this->get_presets();
        $this->reset(['currency', 'description']); 

    }

    public function update()
    {

        $this->validate();
        
        $update = [
            'currency_id' => $this->currency,
            'description' => $this->description,
        ];

        //save details =========== 
        MyCurrenciesModel::where('user_id', $this->get_user_id())->where('id',$this->edit_id)->update($update);
        session()->flash('success-modal','You have updated details successfully.');

    }

    function title(){
        $currency = CurrenciesModel::where('id', $this->currency)->first();
        $this->description = $currency->code . ' ('.$currency->description.')';
    }

    public function delete($id)
    {

        $record = MyCurrenciesModel::where('user_id', $this->get_user_id())->where('id',$id)->delete();
        session()->flash('success','You have deleted the record successfully.');
        $this->get_presets();
        $this->dispatch('close-modal');

    }

    public function showModal($action, $id = ""){

        $this->get_presets();

        switch($action){
            case 'add': 
                $this->modal_title = "Add New";
                $this->form_action = "create";
                $this->btn_text = "Submit";
                $this->dispatch('show-modal');
                break;
            case 'view':
                $this->form_action = "view";
                $this->modal_title = "View Details";
                $this->set_edit_record($id);
                $this->dispatch('show-modal');
                break;
            case 'edit':
                $this->dispatch('close-modal');
                $this->form_action = "update";
                $this->modal_title = "Update Details";
                $this->btn_text = "Save";
                $this->set_edit_record($id);
                $this->dispatch('show-modal');
                break;
            case 'del':
                $record = MyCurrenciesModel::where('id', $id)->first();
                $this->edit_id = $record->id;
                $this->dispatch('show-del-modal');
                break;
        }

    }

    function set_edit_record($id){
        $record = MyCurrenciesModel::where('user_id', $this->get_user_id())->where('id', $id)->first();
        $this->edit_id = $record->id;
        $this->currency = $record->currency_id;
        $this->description = $record->description;
    }

    #[Computed]
    function records(){
        return MyCurrenciesModel::where('user_id', $this->get_user_id())->orderBy('description', 'asc')->paginate($this->records_per_page);
    }


    function get_presets(){
        $this->records();
        $this->currencies = CurrenciesModel::orderBy('code', 'asc')->orderBy('description', 'asc')->get();
    }

    function get_user_id(){
        $user = Auth::user();
        return $user->id;
    }
}
