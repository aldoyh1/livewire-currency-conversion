<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Currencies as CurrenciesModel;
use App\Models\Settings as SettingsModel;
use Livewire\Attributes\Rule; 
use Livewire\WithPagination; 
use Livewire\Attributes\Computed;
use App\Mail\ManagePeopleMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


class SettingsComponent extends Component
{


    use WithPagination;

    #[Rule('required')]
    public $system_name = "";
    
    #[Rule('required')]
    public $description = "";
    
    #[Rule('required')]
    public $value = "";

    public $edit_id = "";
    public $modal_title = "";
    public $btn_text = "";
    public $form_action = "";
    public $currencies = [];
    public $show_form = false;
    public $records_per_page = 10;

    public function mount(){
        $this->get_presets();
    }

    public function render()
    {
        return view('livewire.settings-component')->layout('livewire.layouts.niceadmin-logged-in');
    }

    public function create()
    {

        $this->validate();
        
        $insert = [
            'system_name' => $this->system_name,
            'description' => $this->description,
            'value' => $this->value,
        ];

        $record = SettingsModel::create($insert);
        session()->flash('success-modal','You have added new record successfully.');
        $this->get_presets();
        $this->reset(['system_name', 'description', 'value']);
    }

    public function update()
    {

        $this->validate();
        
        $update = [
            'system_name' => $this->system_name,
            'description' => $this->description,
            'value' => $this->value,
        ];

        //save =========== 
        SettingsModel::where('user_id', $this->get_user_id())->where('id',$this->edit_id)->update($update);
        session()->flash('success-modal','You have updated details successfully.');

    }

    public function delete($id)
    {

        $record = SettingsModel::where('user_id', $this->get_user_id())->where('id',$id)->delete();
        session()->flash('success','You have deleted the record successfully.');
        $this->get_presets();
        $this->dispatch('close-modal');

    }

    public function showModal($action, $id = ""){

        $this->get_presets();

        switch($action){
            case 'add': 
                $this->modal_title = "Add New";
                $this->form_action = "create";
                $this->btn_text = "Submit";
                $this->dispatch('show-modal');
                break;
            case 'view':
                $this->form_action = "view";
                $this->modal_title = "View Details";
                $this->set_edit_record($id);
                $this->dispatch('show-modal');
                break;
            case 'edit':
                $this->dispatch('close-modal');
                $this->form_action = "update";
                $this->modal_title = "Update Details";
                $this->btn_text = "Save";
                $this->set_edit_record($id);
                $this->dispatch('show-modal');
                break;
            case 'del':
                $record = SettingsModel::where('user_id', $this->get_user_id())->where('id', $id)->first();
                $this->edit_id = $record->id;
                $this->dispatch('show-del-modal');
                break;
        }

    }

    function set_edit_record($id){
        $record = SettingsModel::where('user_id', $this->get_user_id())->where('id', $id)->first();
        $this->edit_id = $record->id;
        $this->system_name = $record->system_name;
        $this->description = $record->description;
        $this->value = $record->value;
    }

    #[Computed]
    function records(){
        return SettingsModel::where('user_id', $this->get_user_id())->orderBy('description', 'asc')->paginate($this->records_per_page);
    }


    function get_presets(){
        $this->records();
        $this->currencies = CurrenciesModel::orderBy('description', 'asc')->get();
    }

    function get_user_id(){
        $user = Auth::user();
        return $user->id;
    }

}
