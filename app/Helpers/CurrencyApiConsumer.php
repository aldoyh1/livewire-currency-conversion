<?php

/**
 * Created by PhpStorm.
 * User: slamtony
 * Date: 2022/03/30
 * Time: 20:24
 */


namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class CurrencyApiConsumer
{

    private $base_url;
    private $api_key;

    public function get_currency_list(){
        $endPoint = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies.json";
        $results = $this->file_get_contents($endPoint);
        return $results;
    }
    
    public function convert_currency($amount, $from_currency, $to_currency){
        $from = strtolower($from_currency);
        $to = strtolower($to_currency);
        $endPoint = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/$to/$from.json";
        $results = $this->file_get_contents($endPoint);
        if($results['status']){

            $rate = $results['data']->$from;
            $results['data']->base_amount = $amount;
            $results['data']->converted_amount = round($amount * $rate, 2);
            $results['data']->rate = $rate;
        }

        return $results;
    }

    public function file_get_contents($endPoint){
        $status = ['status' => false, 'message' => ''];
        if (($data = @file_get_contents($endPoint)) === false) {
            $status = ['status' => false, 'message' => 'File get contens failed at: ' . $endPoint];
        } else {
            $json = json_decode($data);
            if (json_last_error() !== 0) {
                // JSON not valid
            }else{
                $status = ['status' => true, 'message' => 'It worked', 'data' => $json];
            }
        }
        return $status;
    }

    public function get_data_by_end_point($endPoint, array $params)
    {

        $base_url = "https://www.amdoren.com/api/";
        $base_url = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/$endPoint.json";
        $base_url = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies.json";
        $api_key = 'r8SriUqXj687AWXeECmL5tNDPAGMgX';
        $status = ['status' => false, 'message' => "", "data" => []];
        $response = Http::get($base_url . $endPoint, $params);
        
        print_r($response);exit;

        $status = ['status' => $response->ok(), "data" => $response->body()];
        return $status;
    }
}
