<?php
/**
 * Created by PhpStorm.
 * User: slamtony
 * Date: 2022/02/16
 * Time: 17:41
 */

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class Functions
{

    public function get_login_redirect_url()
    {
        return session('redirect_url');
    }
    
    public function set_login_redirect_url($url)
    {
        session(['redirect_url' => $url]);
        session()->save();
    }

    public function clear_login_redirect_url()
    {
        session()->forget('redirect_url');
    }

    function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}