<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Mbusi's Currency Conversion

This application was built using the following tools.

- <a  target="_blank" href="https://laravel.com/">Laravel</a>
- <a target="_blank" href="https://livewire.laravel.com/">Livewire</a>
- <a target="_blank" href="https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/">NiceAdmin</a>, basic styling
- <a target="_blank" href="https://github.com/fawazahmed0/currency-api#readme">fawazahmed0 / currency-api</a>, Free Currency Exchange Rates API with 150+ Currencies & No Rate Limits

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Email Setup

You will need to setup your smtp details in the <b>.env</b> file to receive the flowing emails: 

- Creating Initial User on "<i><b>Initial Setup</b></i>"
- User Registration on "<i><b>Register Form</b></i>"
- User Retrieve Password on "<i><b>Forgot Password</b></i>"
- User Update Password on "<i><b>Update Password</b></i>"

## SMTP Variables on your .env file
- <code>MAIL_MAILER=log</code>
- <code>MAIL_HOST=mailpit</code>
- <code>MAIL_PORT=1025</code>
- <code>MAIL_USERNAME=null</code>
- <code>MAIL_PASSWORD=null</code>
- <code>MAIL_ENCRYPTION=null</code>
- <code>MAIL_FROM_ADDRESS="hello@example.com"</code>
- <code>MAIL_FROM_NAME="${APP_NAME}"</code>

## Project Prerequisits
-  <code>
    "require": {
        "php": "^8.1",
        "guzzlehttp/guzzle": "^7.2",
        "laravel/framework": "^10.10",
        "laravel/sanctum": "^3.3",
        "laravel/tinker": "^2.8",
        "livewire/livewire": "^3.0"
    }
    </code> 

## Project Setup

- Create mysql databse called: <i><b>currrency_mbusi_db</b></i> 
- database username: <b>currrencyroot</b>
- database password: <b>mydbpass123</b>
- Update your <b>.env</b> to match these details (should be done for you already)
- Run: <code>composer update</code>
- Run: <code>php artisan optimize:clear</code>
- Run: <code>php artisan system:initial-setup</code>

## Running The Project 
- Run: <code>php artisan serve --port 8007</code>
- You can now access your project on <a href="http://127.0.0.1:8006">http://127.0.0.1:8007</a>
