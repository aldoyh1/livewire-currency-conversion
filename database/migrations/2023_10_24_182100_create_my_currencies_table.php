<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('my_currencies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->index('fk_c_u')->onDelete('cascade');
            $table->foreignId('currency_id')->nullable()->constrained('currencies')->index('fk_c_c')->onDelete('set null');
            $table->string('description', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('my_currencies');
    }
};
