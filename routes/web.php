<?php

use App\Livewire\HomeComponent;
use App\Livewire\SettingsComponent;
use App\Livewire\UserRegisterComponent;
use App\Livewire\UserRetrievePasswordComponent;
use App\Livewire\CurrenciesComponent;
use App\Livewire\MyCurrenciesComponent;
use App\Livewire\UserLoginComponent;
use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CustomCheckLoggedInState;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Middleware\CustomCheckMyCurrenciesRealtionships;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//======= views ========= 
Route::get('/logout', [AppController::class, 'logout'])->name('logout');

//======= we do it components way now ========= 
Route::get('currencies', CurrenciesComponent::class)->middleware(CustomCheckLoggedInState::class)->name('manage-currencies');
Route::get('settings', SettingsComponent::class)->middleware(CustomCheckLoggedInState::class)->name('manage-settings');
Route::get('my-currencies', MyCurrenciesComponent::class)->middleware(CustomCheckLoggedInState::class)->middleware(CustomCheckMyCurrenciesRealtionships::class)->name('my-currencies');
Route::get('login', UserLoginComponent::class)->middleware(RedirectIfAuthenticated::class)->name('login');
Route::get('register', UserRegisterComponent::class)->middleware(RedirectIfAuthenticated::class)->name('register');
Route::get('retrieve-password', UserRetrievePasswordComponent::class)->middleware(RedirectIfAuthenticated::class)->name('retrieve-password');
Route::group(['prefix' => '', 'middleware' => [CustomCheckLoggedInState::class]], function () {
    Route::get('/', HomeComponent::class)->name('landing');
    Route::get('/dashboard', HomeComponent::class)->name('dashboard');
    Route::get('/home', HomeComponent::class)->name('home');
});

