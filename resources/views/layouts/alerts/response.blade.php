@if(Session::has('primary'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-success fade show">
                {{ session('primary') }}
            </div>
        </div>
    </div>
@endif

@if (Session::has('errors'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-danger fade show">
                @foreach (Session::get('errors') as $error)
                    <ul>
                        <li>{{ $error[0] }}</li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
@endif

@if(Session::has('default'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-secondary fade show">
                {{ session('default') }}
            </div>
        </div>
    </div>
@endif

{{-- alert alert-success fade show --}}
{{-- alert alert-success fade show --}}
@if(Session::has('success'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-success fade show">
                {{ session('success') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('danger'))

    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-danger fade show">
                {{ session('danger') }}
            </div>
        </div>
    </div>
    
@endif



@if(Session::has('warning'))

    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-warning fade show">
                <span class="badge badge-pill badge-warning">Warning</span>
                {{ session('warning') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('info'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-info fade show">
                {{ session('info') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('message'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Well done!</h4>
                <p> {{ session('message') }}</p>
            </div>
        </div>
    </div>
@endif

@if(Session::has('error_message'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert alert-warning" role="alert">
                <p> {{ session('error_message') }}</p>
            </div>
        </div>
    </div>
@endif

@if(Session::has('warning_message'))

    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert alert-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="alert-heading">Warning with possible errors</h4>
                <p> {{ session('warning_message') }}</p>
                <hr>
                <p class="mb-0"> Please check the logs to debug errors.</p>
            </div>
        </div>
    </div>
@endif