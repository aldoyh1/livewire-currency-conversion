@if(Session::has('primary-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-success fade show">
                {{ session('primary-modal') }}
            </div>
        </div>
    </div>
@endif

@if (Session::has('errors-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-danger fade show">
                @foreach (Session::get('errors-modal') as $error)
                    <ul>
                        <li>{{ $error[0] }}</li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
@endif

@if(Session::has('default-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-secondary fade show">
                {{ session('default-modal') }}
            </div>
        </div>
    </div>
@endif

{{-- alert alert-success fade show --}}
{{-- alert alert-success fade show --}}
@if(Session::has('success-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-success fade show">
                {{ session('success-modal') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('danger-modal'))

    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-danger fade show">
                {{ session('danger-modal') }}
            </div>
        </div>
    </div>
    
@endif



@if(Session::has('warning-modal'))

    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-warning fade show">
                <span class="badge badge-pill badge-warning">Warning</span>
                {{ session('warning-modal') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('info-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert with-close alert-info fade show">
                {{ session('info-modal') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('message-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Well done!</h4>
                <p> {{ session('message-modal') }}</p>
            </div>
        </div>
    </div>
@endif

@if(Session::has('error_message-modal'))
    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert alert-warning" role="alert">
                <p> {{ session('error_message-modal') }}</p>
            </div>
        </div>
    </div>
@endif

@if(Session::has('warning_message-modal'))

    <div class="row pb-3">
        <div class="col-11 col-md-12">
            <div class="alert alert-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="alert-heading">Warning with possible errors</h4>
                <p> {{ session('warning_message-modal') }}</p>
                <hr>
                <p class="mb-0"> Please check the logs to debug errors.</p>
            </div>
        </div>
    </div>
@endif