    <aside id="sidebar" class="sidebar">
        <ul class="sidebar-nav" id="sidebar-nav">

            <li class="nav-item">
                <a class="nav-link " href="{{ route('dashboard') }}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
                </a>
            </li><!-- End Dashboard Nav -->

            <li class="nav-item">
                <a class="nav-link collapsed"  href="{{ route('manage-currencies') }}">
                <i class="bi bi-card-list"></i>
                <span>Manage Currencies</span>
                </a>
            </li><!-- End Register Page Nav -->

            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ route('my-currencies') }}">
                <i class="bi bi-card-list"></i>
                <span>My Currencies</span>
                </a>
            </li><!-- My Currencies -->


            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ route('manage-settings') }}">
                <i class="bi bi-card-list"></i>
                <span>Settings</span>
                </a>
            </li><!-- Settings -->

            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ route('logout') }}" wire:navigate>
                <i class="bi bi-power"></i>
                <span>Logout</span>
                </a>
            </li><!-- End Register Page Nav -->

        </ul>
    </aside><!-- End Sidebar-->
