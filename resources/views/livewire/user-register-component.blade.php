<div>
    @section('title')
        {{ env('APP_NAME') }} | Register
    @endsection
    <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center">
                <a href="{{ route('login') }}"><img width="250" src="/assets/images/x.png" alt=""></a>
              </div>

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Create an Account</h5>
                  </div>

                  <form class="row g-3" wire:submit="register">
                    <div class="col-12">
                      <label for="yourName" class="form-label">Your Name *</label>
                      <input type="text" name="name" class="form-control" wire:model.blur="name">
                      @error('name')<small class="text-danger">{{ $message }}</small>@enderror 
                    </div>

                    <div class="col-12">
                      <label for="yourSurname" class="form-label">Your Surname *</label>
                      <input type="text" name="surname" class="form-control" wire:model.blur="surname">
                      @error('surname')<small class="text-danger">{{ $message }}</small>@enderror 
                    </div>

                    <div class="col-12">
                      <label for="yourUsername" class="form-label">Email *</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                        <input type="text" name="email" class="form-control" wire:model.blur="email">
                      </div>
                        @error('email')<small class="text-danger">{{ $message }}</small>@enderror 
                    </div>

                    <div class="col-12 mt-2">
                        <label for="yourUsername" class="form-label">Password *</label>
                        <div class="input-group has-validation">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="ri-lock-2-line"></i></span>
                            <input autocomplete="off" type="password" name="password" class="form-control" wire:model.blur="password">
                        </div>
                        @error('password')<small class="text-danger">{{ $message }}</small>@enderror 
                    </div>

                    <div class="col-12 mt-2">
                        <label for="yourUsername" class="form-label">Confirm Password *</label>
                        <div class="input-group has-validation">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="ri-lock-2-line"></i></span>
                            <input autocomplete="off" type="password" name="confirm_password" class="form-control" wire:model.blur="confirm_password">
                        </div>
                        @error('confirm_password')<small class="text-danger">{{ $message }}</small>@enderror 
                    </div>

                    <div class="col-12">
                      <div class="form-check">
                        <input class="form-check-input" name="auto_login" id="auto_login" type="checkbox" value="" wire:model.blur="auto_login">
                        <label class="form-check-label" for="auto_login">Auto login after registration</label>
                        @error('auto_login')<small class="text-danger">{{ $message }}</small>@enderror
                      </div>
                    </div>

                    <div class="col-12">
                      <div class="form-check">
                        <input class="form-check-input" name="terms" id="terms" type="checkbox" value="1" wire:model.blur="terms">
                        <label class="form-check-label" for="terms">I agree and accept the <a target="_blank" href="http://developerexcuses.com/">terms and conditions</a></label>
                        <div class="invalid-feedback">You must agree before submitting.</div>
                        @error('terms')<small class="text-danger">{{ $message }}</small>@enderror
                      </div>
                    </div>
                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit">Create Account</button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Already have an account? <a href="{{route('login')}}" wire:navigate>Log in</a></p>
                    </div>
                  </form>

                </div>
              </div>

              <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Designed by <a target="_blank" href="http://developerexcuses.com/">Mbusi Lukhele</a>
              </div>

            </div>
          </div>
        </div>

      </section>
    </section>
</div>
