<div>
    @section('title')
        {{ env('APP_NAME') }} | Retrieve Password
    @endsection
    <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center">
                <a href="{{ route('login') }}"><img width="250" src="/assets/images/x.png" alt=""></a>
              </div>

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Retrieve Password</h5>
                  </div>

                  <form class="row g-3" wire:submit="register">

                      <hr />
                      <div class="row form-group">
                        <div class="col col-md-12">
                            This feature is currently under construction, please search your emails with subject "<strong>New User Created</strong>".
                        </div>
                      </div>
                      <div class="row form-group pt-3">
                        <div class="col col-md-12">
                            You could find your logins on that email.
                        </div>
                      </div>
                      <hr />
                      <div class="col-12">
                        <p class="small mb-0">Found your login details? <a href="{{route('login')}}" wire:navigate>Log in</a></p>
                      </div>
                  </form>

                </div>
              </div>

              <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Designed by <a target="_blank" href="http://developerexcuses.com/">Mbusi Lukhele</a>
              </div>

            </div>
          </div>
        </div>

      </section>
    </section>
</div>
