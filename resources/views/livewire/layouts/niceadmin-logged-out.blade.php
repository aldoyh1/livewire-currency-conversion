<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        
        {{-- Bootstrap css --}}
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Fontfaces CSS-->
        <link href="/assets/theme/cool-admin-master/css/font-face.css" rel="stylesheet" media="all">
        <link href="/assets/theme/cool-admin-master/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="/assets/theme/cool-admin-master/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="/assets/theme/cool-admin-master/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="/assets/theme/niceadmin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/theme/niceadmin/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="/assets/theme/niceadmin/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="/assets/theme/niceadmin/vendor/quill/quill.snow.css" rel="stylesheet">
        <link href="/assets/theme/niceadmin/vendor/quill/quill.bubble.css" rel="stylesheet">
        <link href="/assets/theme/niceadmin/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="/assets/theme/niceadmin/vendor/simple-datatables/style.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="/assets/theme/niceadmin/css/style.css" rel="stylesheet">

        @livewireStyles
    </head>
    <body>

        <main>
            <div class="container">
                {{ $slot }}
            </div>
        </main><!-- End #main -->

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>


        {{-- Bootstrap scripts --}}
        <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

        <!-- Vendor JS Files -->
        <script src="/assets/theme/niceadmin/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="/assets/theme/niceadmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/theme/niceadmin/vendor/chart.js/chart.umd.js"></script>
        <script src="/assets/theme/niceadmin/vendor/echarts/echarts.min.js"></script>
        <script src="/assets/theme/niceadmin/vendor/quill/quill.min.js"></script>
        <script src="/assets/theme/niceadmin/vendor/simple-datatables/simple-datatables.js"></script>
        <script src="/assets/theme/niceadmin/vendor/tinymce/tinymce.min.js"></script>
        <script src="/assets/theme/niceadmin/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="/assets/theme/niceadmin/js/main.js"></script>

        @stack('scripts')

        @livewireScripts
        
    </body>
</html>
