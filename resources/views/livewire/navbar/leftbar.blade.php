<nav class="navbar-sidebar">
    <ul class="list-unstyled navbar__list">
        <li class="active has-sub">
            <a href="{{route('home')}}" class="js-arrow"><i class="fas fa-tachometer-alt"></i>Dashboard</a>
        </li>
        <li>
            <a href="{{route('currencies.index')}}">
                <i class="fas fa-wrench"></i>Manage Currencies</a>
        </li>
        <li>
            <a href="{{route('my-currencies.index')}}">
                <i class="fas fa-wrench"></i>My Currencies</a>
        </li>
        <li>
            <a href="map.html">
                <i class="fas fa-map-marker-alt"></i>Maps</a>
        </li>
    </ul>
    </nav>