<div>
    <h5 class="card-title">Edit Profile</h5>
    <form wire:submit="update" id="theForm">
        <hr />
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="hf-code" class=" form-control-label  pull-right">Name</label>
            </div>
            <div class="col-12 col-md-5">
                <input autocomplete="off" type="text" name="name" class="form-control" wire:model.blur="name">
                <div>
                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror 
                </div>
            </div>
        </div>
        <hr />
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="hf-code" class=" form-control-label  pull-right">Surname</label>
            </div>
            <div class="col-12 col-md-5">
                <input autocomplete="off" type="text" name="surname" class="form-control" wire:model.blur="surname">
                <div>
                    @error('surname') <span class="text-danger">{{ $message }}</span> @enderror 
                </div>
            </div>
        </div>
        <hr />
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="hf-code" class=" form-control-label  pull-right">Email</label>
            </div>
            <div class="col-12 col-md-5">
                <input autocomplete="off" type="text" name="email" class="form-control" wire:model.blur="email">
                <div>
                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror 
                </div>
            </div>
        </div>
        <hr />
        <div class="row form-group">
            <div class="col col-md-3"></div>
            <div class="col-12 col-md-5">
                <button style="min-width: 120px;" type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-floppy-o "></i> Update
                </button>
            </div>
        </div>
    </form>
    
</div>

