<div>
    <form class="row g-3" wire:submit="calc_exchange">
        @if($prerequisite_error_msg)
            <hr />
            <div class="row">
                <div class="col-lg-12 col-md-12 label ">
                    <div class="alert alert-danger" role="alert">
                        {!! $prerequisite_error_msg !!}
                    </div>
                </div>
            </div>
        @else

            <hr />
            <div class="row">
                <div class="col-lg-3 col-md-4 label ">
                    <input placeholder="Enter amount" autocomplete="off" type="text" name="base_amount" class="form-control" wire:model="base_amount">
                    @error('base_amount')<small class="text-danger">{{ $message }}</small>@enderror
                </div>
                <div class="col-lg-9 col-md-8">
                    <button wire:loading.remove type="submit" class="btn btn-primary"><i class="bi bi-calculator"></i> Convert from {{ $base_currency_desc }} </button>
                    <button wire:loading type="button" disabled class="btn btn-primary disabled"><i class="bi bi-calculator"></i> Please wait...</button>
                </div>

                <div wire:loading.remove>
                    @foreach($my_conversions as $code => $line)
                        <hr />
                        <div class="row" wire:key="{{ $loop->index }}">
                            <div class="col-lg-3 col-md-4 label ">{{ $line['amount'] }}</div>
                            <div class="col-lg-9 col-md-8">{{ $line['description'] }}</div>
                        </div>
                    @endforeach
                </div>

                <div wire:loading>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12 col-md-12 label text-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                            Calculating rates...
                        </div>
                    </div>
                </div>
            </div>

        @endif

        {{-- <hr />
            <div class="row">
            <div class="col-lg-3 col-md-4 label">20</div>
            <div class="col-lg-9 col-md-8">CHF (US Dollar)</div>
        </div> --}}

    </form>
</div>

