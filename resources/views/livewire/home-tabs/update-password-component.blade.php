<div>
    <h5 class="card-title">Change Password</h5>
    @include('layouts.alerts.response-modal')
    <form wire:submit="update" id="theForm">
        <hr />
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="hf-code" class=" form-control-label  pull-right">Current Password</label>
            </div>
            <div class="col-12 col-md-5">
                <input autocomplete="off" type="password" name="current_password" class="form-control" wire:model.blur="current_password">
                <div>
                    @error('current_password') <span class="text-danger">{{ $message }}</span> @enderror 
                </div>
            </div>
        </div>
        <hr />
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="hf-code" class=" form-control-label  pull-right">New Password</label>
            </div>
            <div class="col-12 col-md-5">
                <input autocomplete="off" type="password" name="new_password" class="form-control" wire:model.blur="new_password">
                <div>
                    @error('new_password') <span class="text-danger">{{ $message }}</span> @enderror 
                </div>
            </div>
        </div>
        <hr />
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="hf-code" class=" form-control-label  pull-right">Confirm Password</label>
            </div>
            <div class="col-12 col-md-5">
                <input autocomplete="off" type="password" name="confirm_password" class="form-control" wire:model.blur="confirm_password">
                <div>
                    @error('confirm_password') <span class="text-danger">{{ $message }}</span> @enderror 
                </div>
            </div>
        </div>
        <hr />
        <div class="row form-group">
            <div class="col col-md-3"></div>
            <div class="col-12 col-md-5">
                <button style="min-width: 120px;" type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-floppy-o "></i> Update
                </button>
            </div>
        </div>
    </form>
    
</div>

