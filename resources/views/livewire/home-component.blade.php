
<div>
    @section('title')
        {{ env('APP_NAME') }} | Dashboard
    @endsection

    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    
    <section class="section profile">
      <div class="row">
        <div class="col-xl-12">

          @include('layouts.alerts.response-modal')

          @include('layouts.alerts.response')
          
          <div class="card">
            <div class="card-body pt-3">
              <!-- Bordered Tabs -->
              <ul class="nav nav-tabs nav-tabs-bordered">

                <li class="nav-item">
                  <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#dashboard">Dashboard</button>
                </li>
                
                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
                </li>

                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
                </li>

                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
                </li>

              </ul>
              <div class="tab-content pt-2">

              <div class="tab-pane fade show active dashboard" id="dashboard">
                  <div class="row pt-3">
                    <!-- Sales Card -->
                    <div class="col-xxl-6 col-md-6">
                      <div class="card info-card bg-danger text-white">
                        <div class="card-body">
                          <h5 class="card-title">Currencies <span class="text-white">| Today</span></h5>
                          <div class="d-flex align-items-center">
                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                              <i class="bi bi-currency-exchange"></i>
                              <i class="bi bi-currency-exchange"></i>
                              
                            
                            </div>
                            <div class="ps-3">
                              <h6>{{ $currencies }}</h6>
                              <a href="{{ route('manage-currencies') }}"><span class="text-muted small pt-2 ps-1">View</span></a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div class="col-xxl-6 col-md-6">
                      <div class="card info-card bg-success text-white">
                        <div class="card-body">
                          <h5 class="card-title">My Currencies <span class="text-white">| Today</span></h5>
                          <div class="d-flex align-items-center">
                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                              <i class="bi bi-currency-exchange"></i>
                              <i class="bi bi-currency-exchange"></i>
                              
                            
                            </div>
                            <div class="ps-3">
                              <h6>{{ $my_currencies }}</h6>
                              <a href="{{ route('my-currencies') }}"><span class="text-muted small pt-2 ps-1">View</span></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <!-- Sales Card -->
                    <div class="col-xxl-12 col-md-12">
                      <div class="card info-card bg-dark text-white">
                        <div class="card-body">
                          <h5 class="card-title text-white">Exchange Rate <span>| Today</span></h5>
                          <livewire:home-tabs.home-tab-exchange-calc-component />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade profile-overview" id="profile-overview">
                    <h5 class="card-title">Profile Details</h5>
                    <hr />
                    <div class="row">
                      <div class="col-lg-3 col-md-4 label ">Full Name</div>
                      <div class="col-lg-9 col-md-8">{{ ucfirst($name) }} {{ ucfirst($user->surname) }} </div>
                    </div>
                    <hr />

                    <div class="row">
                      <div class="col-lg-3 col-md-4 label">Email</div>
                      <div class="col-lg-9 col-md-8">{{ $user->email }}</div>
                    </div>
                    <hr />

                    <div class="row">
                      <div class="col-lg-3 col-md-4 label">Created</div>
                      <div class="col-lg-9 col-md-8">{{ $user->created_at }}</div>
                    </div>
                    <hr />

                    <div class="row">
                      <div class="col-lg-3 col-md-4 label">Last Login</div>
                      <div class="col-lg-9 col-md-8">{{ $user->last_login_at }}</div>
                    </div>

                </div>


                <div class="tab-pane fade profile-edit pt-3" id="profile-edit">
                  <livewire:home-tabs.home-tab-update-profile-component />
                </div>

                <div class="tab-pane fade pt-3" id="profile-change-password">
                  <!-- Change Password Form -->
                  <livewire:home-tabs.home-tab-update-password-component />
                </div>

              </div><!-- End Bordered Tabs -->

            </div>
          </div>

        </div>
      </div>
    </section>
</div>
