
<div>
    @section('title')
        {{ env('APP_NAME') }} | My Currencies
    @endsection

    <div class="pagetitle">
        <h1>My Currencies</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item active">My Currencies</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    
    <section class="section dashboard">
        <div class="containerSuspended">
            <div class="row">
                <div class="col-11 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <button wire:click="showModal('add')" style="float: right;" type="button" class="btn btn-primary">
                                Add new
                            </button>
                        </div>
                        <div class="card-body card-block">
                            @include('layouts.alerts.response')

                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Description</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($this->records as $record)
                                        <tr>
                                            <td width="10%">{{ $record->currency->code }}</td>
                                            <td >{{ $record->description }}</td>
                                            <td width="25%" class="text-center">
                                                <div class="table-data-feature">
                                                    <button class="btn btn-secondary" wire:click="showModal('view','{{$record->id}}')" class="item" data-toggle="tooltip" data-placement="top" title="View">
                                                        <i class="zmdi zmdi-eye"></i>
                                                    </button>
                                                    <button class="btn btn-secondary" wire:click="showModal('edit','{{$record->id}}')" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="btn btn-secondary" wire:click="showModal('del','{{$record->id}}')" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr><td class="text-center" colspan="9">No results found</td></tr>
                                    @endforelse
                                </tbody>
                            </table>

                            <div class="row pull-right">
                                {{ $this->records->links() }}
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- @section('modals') --}}
    <!-- modal medium -->
        <div wire:ignore.self class="modal fade" id="AddEditModal" tabindex="-1" aria-labelledby="AddEditModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="AddEditModalLabel">{{ $modal_title }}</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @include('layouts.alerts.response-modal')
                    <form wire:submit="{{ $form_action }}" id="theForm">
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="currency" class=" form-control-label pull-right">Currency </label>
                            </div>
                            <div class="col-12 col-md-5">
                                <select autocomplete="off" wire:change="title" {{ $form_action == 'view' ? 'disabled="disabled"' : '' }} id="currency" name="currency" wire:model.blur="currency" class="form-control">
                                    <option value="">Please Select</option>
                                    @foreach($currencies as $line)
                                        <option {{ ($line->id == $currency ? "selected" : "") }} wire:key="{{ $loop->index }}" value="{{ $line->id }}">{{ $line->code }} - {{ $line->description }} </option>
                                    @endforeach
                                </select>
                                <div>
                                    @error('language') <span class="text-danger">{{ $message }}</span> @enderror 
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="hf-email" class=" form-control-label  pull-right">Description</label>
                            </div>
                            <div class="col-12 col-md-5">
                                <input autocomplete="off" {{ $form_action == 'view' ? 'disabled="disabled"' : '' }} autocomplete="off" type="text" id="description" name="description" class="form-control" wire:model.blur="description">
                                <div>
                                    @error('description') <span class="text-danger">{{ $message }}</span> @enderror 
                                </div>
                            </div>
                        </div>
                        @if(in_array($form_action, ['view']))
                            {{-- not working for some reason --}}
                            {{-- <hr />
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="hf-email" class=" form-control-label pull-right"></label>
                                </div>
                                <div class="col-12 col-md-5">
                                    <button class="btn btn-secondary" wire:click="showModal('edit','{{$edit_id}}')" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i> Edit
                                    </button>
                                </div>
                            </div> --}}
                        @else
                            <hr />
                            <div class="row form-group">
                                <div class="col col-md-3"></div>
                                <div class="col-12 col-md-5">
                                    <button style="min-width: 120px;" type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-floppy-o "></i> {{ $btn_text }}
                                    </button>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
        <!-- delete -->
        <div  class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form wire:submit="add">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Perform Delete</h5>
                        </div>
                        <div class="modal-body">
                            
                            <div class="card-body card-block">
                                    
                                    <div class="row form-group">
                                        <div class="col col-md-12">
                                            <p>Are you sure you want to delete this record?</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-12">
                                            <button type="button" class="btn btn-secondary" wire:click="delete('{{ $edit_id }}')"><i class="zmdi zmdi-delete"></i> Yes, Continue Delete</button>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- delete -->
    {{-- @endsection --}}
    @push('scripts')
        <script>
            document.addEventListener('livewire:initialized', () => {
                $(function(){
                    $('.datepicker').datepicker({ format: 'yyyy-mm-dd', autoclose: true});
                    $('#dob2').change(function(){
                        //$('#dob').val($('#dob2').val());
                        //@this.set('dob', $('#dob2').val());
                    });
                });
            });
            window.addEventListener
            document.addEventListener('livewire:initialized', () => {
                @this.on('show-modal', (event) => {
                    $('#AddEditModal').modal('show');
                });
                @this.on('show-del-modal', (event) => {
                    $('#delModal').modal('show');
                });
                @this.on('close-modal', (event) => {
                    $('#AddEditModal').modal('hide');
                    $('#delModal').modal('hide');
                });
            });
            </script>
        @endpush
</div>
