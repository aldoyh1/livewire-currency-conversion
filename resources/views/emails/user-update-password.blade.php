<x-mail::message>
# Introduction

Dear {{ $data['name'] }} {{ $data['surname'] }},

See your new password updated. 

<p>Email: {{ $data['email'] }}</p>
<p>Password: {{ $data['password'] }}</p>
 
<x-mail::button url="{{ $data['login_link'] }}">
Login here
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
